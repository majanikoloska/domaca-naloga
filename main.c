#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define MAX_WRONG_GUESSES 10 // največje število napačnih ugibanj, preden igralec izgubi

const char *besede[] = {"miza", "okno", "banana", "bozic", "slika", "racunalnik", "juha", "obleka"}; // niz moznih besed

// prototipi funkcij 
void narisiObesenjaka (int napacneOcene);

int main() {
   srand(time(0)); // posadite generator nakljucnih stevil.
   int numWords = sizeof(besede) / sizeof(besede[0]); // stevilo besed v polju
   int index = rand() % numWords; // izberite nakljucno besedo iz polja
   const char *beseda = besede [index]; // izbrana beseda
   int dolzinaBesede = strlen(beseda); // dolzina besede
   char ugibanjeBesed [dolzinaBesede + 1]; // polje za ugibanje besed (en dodaten presledek za ničelno besedo)
   char ugibanjeBesed [dolzinaBesede + 1]; // polje za ugibanje besed (en dodaten presledek za ničelni zaključek)

      for (int i = 0; i < dolzinaBesede; i++) {
         ugibanjeBesed[i] = '_'; // inicializirajte polje za ugibanje besed s podcrtankami
        }

   ugibanjeBesed[dolzinaBesede] = '\0'; // null-terminate polje za ugibanje besed
   int napacneOcene = 0; // število napačnih ugibanj 
   char crka; // crka ki jo vnese igralec. 
   int zeUganili = 0; // za preverjanje ali je bila crka ze uganjena.
   printf("Dobrodošli v Hangmanu!\n");

    while (napacneOcene < MAX_WRONG_GUESSES){
       zeUganili = 0;
       narisiObesenjaka (napacne ocene); // narisi obesenca
       printf("Beseda: %s\n", ugibanjeBesed); // natisne polje za ugibanje besed
       printf("Ostale ugibanja: %d\n", MAX_WRONG_GUESSES - napacneOcene);// izpise stevilo preostalih ugibanj 
       printf("Vnesite črko: "); 
       scanf("%c", &crka); // preberi pismo igralca

        for (int i = 0; i < dolzinaBesede; i++){
            if (beseda[i] = crka){ // ce je crka v besedi
               scanf("%c", &crka); // preberi pismo igralca

                for (int i = 0; i < dolzinaBesede; i++) { 
                     if (beseda[i] crka) { // ce je crka v besedi

                       ugibanjeBesed[i] = crka; // Izpolnite prazen prostor
                       zeUganili = 1; // nastavi zastavico ki oznacuje, da je bila crka uganjena.

                    }
                }

            } 
          if (!zeUganili){ // ce crka ni bila uganjena
             napacneOcene++; // povecanje stevila napacnih ugibanj
            } 

             if (strcmp(beseda, ugibanjeBesed) == 0) // ce je bila beseda v celoti uganjena 
                 {
                  narisiObesenjaka (napacneOcene); // narisi obesenca 
                  printf("Čestitamo, zmagali ste! Beseda je bila %s.\n", beseda);
                  return 0; // izhod iz programa
                }

        }

       narisiObesenjaka(napacneOcene);// narisi obesenca
       printf("Čestitamo, zmagali ste! Beseda je bila %s.\n", beseda);

    }
}

void narisiObesenjaka (int napacne ocene ){
   if (napacneOcene >= 1)
      printf(" |\n");
   else
      printf("");

   if (napacneOcene >= 2) 
      printf("|\n");
   else
      printf("");

   if (napacneOcene >= 3) 
      printf(" O\n"); 
   else
      printf("");

   if (napacneOcene == 4)
      printf("/ \n");

   if (napacneOcene == 5)
      printf("/| \n");

   if (napacneOcene >= 6) 
      printf("/|\\ \n");  
   else
      printf("");

   if (napacneOcene >= 7) 
      printf(" | \n");
   else
      printf("");

   if (napacneOcene == 8)
      printf("");

   if (napacneOcene >= 9) 
      printf("/ \\ \n");  
   else
      printf("");
}


